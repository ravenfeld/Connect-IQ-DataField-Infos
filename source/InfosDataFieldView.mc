
using Toybox.WatchUi as Ui;
using Toybox.Application as App;
using Toybox.Graphics as Gfx;
using Toybox.Attention as Attention;
using Toybox.FitContributor as Fit;

class InfosDataFieldView extends Ui.DataField
{
	hidden var data_field_1;
    hidden var two_datafield; 	
	hidden var gps;
	hidden var old_battery_low=0;
	hidden var old_gps_low=0;

    hidden var batteryField = null;
	const BATTERY_FIELD_ID = 0;
    hidden var gpsField = null;
	const GPS_FIELD_ID = 1;
    
    function initialize() {
        DataField.initialize();
        batteryField = createField(
            Ui.loadResource(Rez.Strings.battery_level),
            BATTERY_FIELD_ID,
            FitContributor.DATA_TYPE_FLOAT,
            {:mesgType=>Fit.MESG_TYPE_RECORD, :units=>"%"}
        );
        gpsField = createField(
            Ui.loadResource(Rez.Strings.gps_level),
            GPS_FIELD_ID,
            FitContributor.DATA_TYPE_SINT8,
            {:mesgType=>Fit.MESG_TYPE_RECORD}
        );
    }
    
    function compute(info)
    {
        gps = info.currentLocationAccuracy;
        if(gps!=null){
        	gpsField.setData(gps);
        }
        batteryField.setData(System.getSystemStats().battery);
    }
    
    function onUpdate(dc){ 
        var flag = getObscurityFlags();
        data_field_1 = flag==OBSCURE_BOTTOM+OBSCURE_TOP+OBSCURE_RIGHT+OBSCURE_LEFT||flag==0;
        var battery_display =  App.getApp().getProperty("battery_display");
    	var gps_display =  App.getApp().getProperty("gps_display");
		two_datafield = battery_display&&gps_display;
		var horizontal = dc.getWidth()/2.0 > dc.getHeight();
        var x_battery = dc.getWidth()/2;
        var x_gps = dc.getWidth()/2;
        var y_battery = dc.getHeight()/2;
        var y_gps = dc.getHeight()/2;
        var width_battery;
        var height_battery;
        var width_gps;
        var height_gps;
        if(two_datafield){
			if(horizontal){
		 		width_battery=dc.getWidth()/2;  
	    		height_battery=dc.getHeight();
	    		width_gps=dc.getWidth()/2;  
	    		height_gps=dc.getHeight();
	    	}else{
	    		width_battery=dc.getWidth();  
	    		width_gps=dc.getWidth();
	    		height_battery=dc.getHeight()/2;  
	    		height_gps=dc.getHeight()/2; 
	    	}
    	}else{
    		width_battery=dc.getWidth();  
	    	width_gps=dc.getWidth();
	    	height_battery=dc.getHeight();  
	    	height_gps=dc.getHeight(); 
    	}	
               
        if(two_datafield && horizontal){
    		x_battery = dc.getWidth()/4;
    		x_gps = dc.getWidth()*3/4;
    	}
    	if(two_datafield && !horizontal){
			y_battery = dc.getHeight()/4;
			y_gps = dc.getHeight()*3/4;
		}

        if(!data_field_1){
            if(flag==OBSCURE_RIGHT+OBSCURE_LEFT){
            	if(two_datafield){
	        		var translate_x;
	        		translate_x=dc.getWidth()*0.05;
	        		x_battery = x_battery+translate_x;
	        		width_battery = width_battery-translate_x*2;
	        		x_gps = x_gps-translate_x;
	        		width_gps = width_gps-translate_x*2;
        		}
        		flag=0;
        	}
        	if(flag>=OBSCURE_BOTTOM){
				var translate_y;
				if(horizontal && two_datafield){
	        		if(dc.getWidth()<dc.getHeight()*2.1){
	        			translate_y=dc.getHeight()*0.10;
	        		}else{
	        			translate_y=dc.getHeight()*0.15;
	        		}
					y_battery = y_battery-translate_y;
	    			height_battery = height_battery-translate_y*2;
	    			y_gps = y_gps-translate_y;
	    			height_gps = height_gps-translate_y*2;
	        		
        		}else if(two_datafield){
        			translate_y=dc.getHeight()*0.05;	
        			y_battery = y_battery-translate_y;
	        		height_battery = height_battery-translate_y*2;
	        		y_gps = y_gps-translate_y*3;
	        		height_gps = height_gps-translate_y*2;
        		}else{
        			translate_y=dc.getHeight()*0.1;	
        			y_battery = y_battery-translate_y;
	        		height_battery = height_battery-translate_y*2;
	        		y_gps = y_gps-translate_y;
	        		height_gps = height_gps-translate_y*2;
        		}
        		        		
        		flag=flag-OBSCURE_BOTTOM;
        	}
        	if(flag==OBSCURE_RIGHT+OBSCURE_LEFT || flag==OBSCURE_RIGHT+OBSCURE_LEFT+OBSCURE_TOP){
        		if(two_datafield){
        			var translate_x;
	        		if(dc.getWidth()<dc.getHeight()*2.1){
	        			translate_x=dc.getWidth()*0.05;
	        		}else{
	        			translate_x=dc.getWidth()*0.1;
	        		}
	        		x_battery = x_battery+translate_x;
	        		width_battery = width_battery-translate_x*2;
	        		x_gps = x_gps-translate_x;
	        		width_gps = width_gps-translate_x*2;
        		}
        		flag=flag-(OBSCURE_RIGHT+OBSCURE_LEFT);
        	}
        	if(flag>=OBSCURE_RIGHT){
        		var translate_x;
        		if(horizontal && two_datafield){
        			if(dc.getWidth()<dc.getHeight()*2){
        				translate_x=dc.getWidth()*0.01;
        			}else{
        				translate_x=dc.getWidth()*0.04;
        			}
	        		x_battery = x_battery-translate_x;
	        		width_battery = width_battery-translate_x*2;
	        		x_gps = x_gps-translate_x*3;
	        		width_gps = width_gps-translate_x*2;
				}else if(two_datafield) {
	 				translate_x=dc.getWidth()*0.15;
		        	x_battery = x_battery-translate_x;
		        	width_battery = width_battery-translate_x*2;
		        	x_gps = x_gps-translate_x;
		        	width_gps = width_gps-translate_x*2;
				}else{
					translate_x=dc.getWidth()*0.1;
		        	x_battery = x_battery-translate_x;
		        	width_battery = width_battery-translate_x*2;
		        	x_gps = x_gps-translate_x;
		        	width_gps = width_gps-translate_x*2;
				}
	        		        		      		
        		flag=flag-OBSCURE_RIGHT;
        	}
        	if(flag>=OBSCURE_TOP){				
				var translate_y;
				if(horizontal && two_datafield){
	        		if(dc.getWidth()<dc.getHeight()*2.1){
	        			translate_y=dc.getHeight()*0.10;
	        		}else{
	        			translate_y=dc.getHeight()*0.15;
	        		}
					y_battery = y_battery+translate_y;
	    			height_battery = height_battery-translate_y*2;
	    			y_gps = y_gps+translate_y;
	    			height_gps = height_gps-translate_y*2;
	        		
        		}else if(two_datafield){
        			translate_y=dc.getHeight()*0.05;	
        			y_battery = y_battery+translate_y*3;
	        		height_battery = height_battery-translate_y*2;
	        		y_gps = y_gps+translate_y;
	        		height_gps = height_gps-translate_y*2;
        		}else{
       				translate_y=dc.getHeight()*0.15;	
        			y_battery = y_battery+translate_y;
	        		height_battery = height_battery-translate_y*2;
	        		y_gps = y_gps+translate_y;
	        		height_gps = height_gps-translate_y*2;
        		}
        		        		        	
        		flag=flag-OBSCURE_TOP;
        	}
        	if(flag>=OBSCURE_LEFT){
        		var translate_x;
        		if(horizontal && two_datafield){
        			if(dc.getWidth()<dc.getHeight()*2){
        				translate_x=dc.getWidth()*0.01;
        			}else{
        				translate_x=dc.getWidth()*0.04;
        			}
	        		x_battery = x_battery+translate_x*3;
	        		width_battery = width_battery-translate_x*2;
	        		x_gps = x_gps+translate_x;
	        		width_gps = width_gps-translate_x*2;
	        	}else if(two_datafield){
        			translate_x=dc.getWidth()*0.15;
        			x_battery = x_battery+translate_x;
        			width_battery = width_battery-translate_x*2;
        			x_gps = x_gps+translate_x;
        			width_gps = width_gps-translate_x*2;
        		}else{
        			translate_x=dc.getWidth()*0.1;
        			x_battery = x_battery+translate_x;
        			width_battery = width_battery-translate_x*2;
        			x_gps = x_gps+translate_x;
        			width_gps = width_gps-translate_x*2;
        		}
        		
        		flag=flag-OBSCURE_LEFT;
        	}
        }

		var color = (getBackgroundColor() == Graphics.COLOR_BLACK) ? Graphics.COLOR_WHITE : Graphics.COLOR_BLACK;
        dc.setColor( color, Gfx.COLOR_TRANSPARENT );
        dc.clear(); 
    	if(battery_display){    	
			drawBattery(x_battery.toNumber(),y_battery.toNumber(),width_battery.toNumber(),height_battery.toNumber(),dc);
		}
		if(gps_display){
			drawGPS(x_gps.toNumber(),y_gps.toNumber(),width_gps.toNumber(),height_gps.toNumber(),dc);
		}
    }
        
    function drawBattery(x,y,width,height,dc) {
    		   
        var color =  getColor(App.getApp().getProperty("battery_color"), getTextColor());
    	var color_low = getColor(App.getApp().getProperty("battery_color_low"), Graphics.COLOR_RED);
		var color_text_default = getColor(App.getApp().getProperty("battery_color_text"), getTextColor());
		var color_text_low = getColor(App.getApp().getProperty("battery_color_text_low"), Graphics.COLOR_RED);
		var color_text = color_text_default;
		var scale_property = App.getApp().getProperty("battery_scale");
	
       	var font = Graphics.FONT_SMALL;
       	if (data_field_1 && scale_property == 0 ){
       		font = Graphics.FONT_TINY;
       	}else if (data_field_1 && scale_property == 1){
       		font = Graphics.FONT_SMALL;
        }else if (data_field_1 && scale_property == 2 ){
       		font = Graphics.FONT_MEDIUM;
       	}else if (data_field_1 && scale_property == 3){
       		font = Graphics.FONT_LARGE;
        }else if (data_field_1 && scale_property == 4){
       		font = Graphics.FONT_NUMBER_MILD;
        }else if (data_field_1 && scale_property == 5){
       		font = Graphics.FONT_NUMBER_MEDIUM;
        }
       
        var size_text = dc.getTextDimensions("888%", font);
        var size_w = size_text[0]+5;
        var size_h = size_text[1];
        if(size_h>height || (size_text[0]+5+size_text[0]/15)>width){        
        	font=Graphics.FONT_TINY;
        	size_text = dc.getTextDimensions("888%", font);
       		size_w = size_text[0]+5;
       		size_h = size_text[1];
        }
        
        var removeRectangle=false; 
        if(two_datafield && (width<(size_text[0]+15+size_text[0]/15) ||  height<size_text[1]+10)){
        	removeRectangle=true;
        }
                
        if( !removeRectangle){           
        	dc.setColor( color, Gfx.COLOR_TRANSPARENT );

        	dc.drawRectangle(x-size_w/2, y- size_h/2, size_w, size_h);
        	dc.fillRectangle(x+size_w/2  - 1, y-size_h/4, size_w/15, size_h/2 ); 
        }
        var battery =System.getSystemStats().battery;         
       	var battery_low =  App.getApp().getProperty("battery_low");
    	if(battery<=battery_low){
    		var battery_alarm =  App.getApp().getProperty("battery_alarm");
    		if(battery_alarm && old_battery_low!=battery_low){
    			old_battery_low=battery_low;
    			alarm();
    		}
    		color_text = color_text_low;
            dc.setColor(color_low, Graphics.COLOR_TRANSPARENT);
        }
        
        var display_percentage = App.getApp().getProperty("battery_percentage");
        var charging=false;
        if(System.Stats has :charging){
        	charging=System.getSystemStats().charging;
        }
        if(charging && !removeRectangle){
        	var charging_poly = [ [x-size_w/3,y],[x+2,y-size_h/4],[x+2,y],[x+size_w/3,y],[x-2,y+size_h/4],[x-2,y]   ];
        	dc.fillPolygon(charging_poly);
        }else if(removeRectangle || display_percentage){
        	dc.setColor(color_text, Graphics.COLOR_TRANSPARENT);
            dc.drawText(x , y, font, format("$1$%", [battery.format("%d")]),Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER); 
         }else{
			dc.fillRectangle(x-size_w/2 + 1, y - size_h/2+ 1, (size_w-2) *battery / 100, size_h - 2);
        }
    }
        
    function drawGPS(x,y,width,height,dc){
    	            
		var scale_property = App.getApp().getProperty("gps_scale");
		var scale;

		if(data_field_1 && scale_property==1){
			scale=1.4;
		}else if(data_field_1 && scale_property==2){
			scale=1.6;
		}else {
			scale=1;
 		}
		
		var size_text_small = dc.getTextDimensions("888%", Graphics.FONT_SMALL);
		
		if(two_datafield && width>(size_text_small[0]+5+size_text_small[0]/15) &&  height>size_text_small[1]*2){
			width = (size_text_small[0]*2).toNumber();
        	height = (size_text_small[1]*2).toNumber();
		}else if(width<height ){
			width=(width*1.25).toNumber();
			height=width;
		}else if(width>height*2.5){
			width=(width*0.5).toNumber();
		}
	
        var bar_height = height/2;
        
        var size_w = width/8.0 * scale;
        var size_h = bar_height/4 ;
        var space = size_w/4;
                
        y = y -bar_height/2;
        
        var color = getColor(App.getApp().getProperty("gps_color"), getTextColor());
        dc.setColor(color, Graphics.COLOR_TRANSPARENT);
        
    	if(gps!=null){
    		var gps_low =  App.getApp().getProperty("gps_low");
    		var gps_alarm =  App.getApp().getProperty("gps_alarm");
    		if( gps_alarm && gps_low >= gps && old_gps_low != gps){
    			old_gps_low = gps;
    			alarm();
    		}
    		if( gps == 4){
    			dc.fillRectangle(x+size_w+space/2+space, y, size_w, bar_height);
			}
			if(gps >=3 ) {
				dc.fillRectangle(x+space/2, y+size_h, size_w, bar_height-size_h);
			}
			if(gps >= 2 ) {
				dc.fillRectangle(x-size_w-space/2, y+2*size_h, size_w, bar_height-2*size_h);
			}
			if(gps >= 1 ) {
				dc.fillRectangle(x-size_w*2-space/2-space, y+3*size_h, size_w, bar_height-3*size_h);			
			}
		}
		dc.drawRectangle(x-size_w*2-space/2-space, y+3*size_h, size_w, bar_height-3*size_h);
		dc.drawRectangle(x-size_w-space/2, y+2*size_h, size_w, bar_height-2*size_h);
		dc.drawRectangle(x+space/2, y+size_h, size_w, bar_height-size_h);
		dc.drawRectangle(x+size_w+space/2+space, y, size_w, bar_height);
    }
        
    function getColor(color_property, color_default){
        if (color_property == 1) {
        	return Gfx.COLOR_BLUE;
        }else if (color_property == 2) {
        	return Gfx.COLOR_DK_BLUE;
        }else if (color_property == 3) {
        	return Gfx.COLOR_GREEN;
        }else if (color_property == 4) {
        	return Gfx.COLOR_DK_GREEN;
        }else if (color_property == 5) {
        	return Gfx.COLOR_LT_GRAY;
        }else if (color_property == 6) {
        	return Gfx.COLOR_DK_GRAY;
        }else if (color_property == 7) {
        	return Gfx.COLOR_ORANGE;
        }else if (color_property == 8) {
        	return Gfx.COLOR_PINK;
        }else if (color_property == 9) {
        	return Gfx.COLOR_PURPLE;
        }else if (color_property == 10) {
        	return Gfx.COLOR_RED;
        }else if (color_property == 11) {
        	return Gfx.COLOR_DK_RED;
        }else if (color_property == 12) {
        	return Gfx.COLOR_YELLOW;
        }
        return color_default;
    }
    
    function getTextColor(){
    	return (getBackgroundColor() == Graphics.COLOR_BLACK) ? Graphics.COLOR_WHITE : Graphics.COLOR_BLACK;
    }
    
    function alarm(){
		if (Attention has :vibrate) {
			var vibrateData = [
				new Attention.VibeProfile(  25, 100 ),
				new Attention.VibeProfile(  50, 100 ),
				new Attention.VibeProfile(  75, 100 ),
				new Attention.VibeProfile( 100, 100 ),
				new Attention.VibeProfile(  75, 100 ),
				new Attention.VibeProfile(  50, 100 ),
				new Attention.VibeProfile(  25, 100 )
			];

			Attention.vibrate(vibrateData);
		} 
	}
}
